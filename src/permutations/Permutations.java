

package permutations;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.util.ArrayList;
import permutations.ClassPermute.Entrada;


public class Permutations {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        

        String inicio = new String();
        String Output = new String();
        ArrayList<String> Prop = new ArrayList();
        ArrayList<String> Affix = new ArrayList();
        int Tab = 11;
        int OrCount = 2;
        Entrada u = new Entrada(Affix,Prop,Tab,OrCount);
        ClassPermute teste = new ClassPermute();
        
        //Jewels
        Prop = new ArrayList();
        Prop.add("ItemClass:Jewels|Abyss Jewels");
        Prop.add("Veiled:0");
        Affix = new ArrayList();
        Affix.add("# increased maximum Life:1");
        Affix.add("# increased maximum Energy Shield:1");
        Affix.add("# to maximum Life:1");
        Affix.add("# to maximum Energy Shield:1");
        Affix.add("Players have a # chance to gain Onslaught on Kill For 4 seconds:1");
        Affix.add("# chance to gain Phasing for 4 seconds on Kill:1");
        OrCount = 1;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);

        //Jewels Corrupted
        Prop = new ArrayList();
        Prop.add("ItemClass:Jewels|Abyss Jewels");
        Prop.add("Veiled:0");
        Prop.add("Corrupted:1");
        Affix = new ArrayList();

        OrCount = 1;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        
       //NEVERSINK Identified Mod Filtering
       
        //Jewels
        Prop = new ArrayList();
        Prop.add("ItemClass:Jewels");
        Affix = new ArrayList();
        Affix.add("#% increased maximum Life:1");
        Affix.add("#% increased maximum Energy Shield:1");
        Affix.add("#% to Global Critical Strike Multiplier:1");
        Affix.add("#% increased Damage over Time:1");
        Affix.add("#% increased Area Damage:1");
        Affix.add("#% increased Attack Speed:1");
        Affix.add("#% increased Attack and Cast Speed:1");
        Affix.add("#% to all Elemental Resistances:1");
        Affix.add("#% increased Melee Damage:1");
        Affix.add("#% increased Projectile Damage:1");
        Affix.add("#% increased Damage:1");
        Affix.add("#% increased Cast Speed:1");
        Affix.add("#% to Critical Strike Multiplier with One Handed Melee Weapons:1");
        Affix.add("#% to Critical Strike Multiplier with Two Handed Melee Weapons:1");
        Affix.add("#% to Critical Strike Multiplier while Dual Wielding:1");
        Affix.add("#% to Critical Strike Multiplier with Fire Skills:1");
        Affix.add("#% to Critical Strike Multiplier with Cold Skills:1");
        Affix.add("#% to Critical Strike Multiplier with Lightning Skills:1");
        Affix.add("#% to Critical Strike Multiplier with Elemental Skills:1");
        Affix.add("#% to Critical Strike Multiplier for Spells:1");
        Affix.add("Minions have #% increased maximum Life:1");
        Affix.add("Minions have #% to all Elemental Resistances:1");
        Affix.add("Minions deal #% increased Damage:1");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        
        //AMULETS ATTACK 3 MOD LIFE
        Prop = new ArrayList();
        Prop.add("ItemClass:Amulets");
        Affix = new ArrayList();
        //Need Life Roll
        Affix.add("# to maximum Life:70:0");
        // Any 2 other
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        Affix.add("# to Accuracy Rating:251");
        Affix.add("#% increased Elemental Damage with Attack Skills:31");
        Affix.add("Adds # to # Fire Damage to Attacks:29");
        Affix.add("Adds # to # Cold Damage to Attacks:25");
        Affix.add("Adds # to # Lightning Damage to Attacks:35");
        Affix.add("Adds # to # Physical Damage to Attacks:14");
        Affix.add("#% to all Elemental Resistances:15");
        Affix.add("# to all Attributes:25");
        Affix.add("#% increased Global Critical Strike Chance:30");
        Affix.add("#% to Global Critical Strike Multiplier:30");
        Affix.add("#% increased maximum Energy Shield:20");
        Affix.add("#% increased Evasion Rating:20");
        Affix.add("#% increased Armour:20");
        
        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //AMULETS ATTACK 3 MOD LIFE OPEN CRAFT
        Prop = new ArrayList();
        Prop.add("ItemClass:Amulets");
        Prop.add("PrefixCount:2");
        Affix = new ArrayList();
        //Need Life Roll
        Affix.add("# to maximum Life:0:0");
        // Any 2 other
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        Affix.add("# to Accuracy Rating:251");
        Affix.add("#% increased Elemental Damage with Attack Skills:31");
        Affix.add("Adds # to # Fire Damage to Attacks:29");
        Affix.add("Adds # to # Cold Damage to Attacks:25");
        Affix.add("Adds # to # Lightning Damage to Attacks:35");
        Affix.add("Adds # to # Physical Damage to Attacks:14");
        Affix.add("#% to all Elemental Resistances:15");
        Affix.add("# to all Attributes:25");
        Affix.add("#% increased Global Critical Strike Chance:30");
        Affix.add("#% to Global Critical Strike Multiplier:30");
        Affix.add("#% increased maximum Energy Shield:20");
        Affix.add("#% increased Evasion Rating:20");
        Affix.add("#% increased Armour:20");
        
        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //AMULETS ATTACK 3 MOD ES 
        Prop = new ArrayList();
        Prop.add("ItemClass:Amulets");
        Affix = new ArrayList();
        //Need ES Roll
        Affix.add("# to maximum Energy Shield:44:0");
        // Any 2 other
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        Affix.add("# to Accuracy Rating:251");
        Affix.add("#% increased Elemental Damage with Attack Skills:31");
        Affix.add("Adds # to # Fire Damage to Attacks:29");
        Affix.add("Adds # to # Cold Damage to Attacks:25");
        Affix.add("Adds # to # Lightning Damage to Attacks:35");
        Affix.add("Adds # to # Physical Damage to Attacks:14");
        Affix.add("#% to all Elemental Resistances:15");
        Affix.add("# to all Attributes:25");
        Affix.add("#% increased Global Critical Strike Chance:30");
        Affix.add("#% to Global Critical Strike Multiplier:30");
        Affix.add("#% increased maximum Energy Shield:20");
        Affix.add("#% increased Evasion Rating:20");
        Affix.add("#% increased Armour:20");
        
        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //AMULETS ATTACK 3 MOD ES OPEN CRAFT
        Prop = new ArrayList();
        Prop.add("ItemClass:Amulets");
        Prop.add("PrefixCount:2");
        Affix = new ArrayList();
        //Need ES Roll
        Affix.add("# to maximum Energy Shield:0:0");
        // Any 2 other
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        Affix.add("# to Accuracy Rating:251");
        Affix.add("#% increased Elemental Damage with Attack Skills:31");
        Affix.add("Adds # to # Fire Damage to Attacks:29");
        Affix.add("Adds # to # Cold Damage to Attacks:25");
        Affix.add("Adds # to # Lightning Damage to Attacks:35");
        Affix.add("Adds # to # Physical Damage to Attacks:14");
        Affix.add("#% to all Elemental Resistances:15");
        Affix.add("# to all Attributes:25");
        Affix.add("#% increased Global Critical Strike Chance:30");
        Affix.add("#% to Global Critical Strike Multiplier:30");
        Affix.add("#% increased maximum Energy Shield:20");
        Affix.add("#% increased Evasion Rating:20");
        Affix.add("#% increased Armour:20");
        
        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        
        //AMULETS ATTACK 4 MOD LIFE OPEN CRAFT
        Prop = new ArrayList();
        Prop.add("ItemClass:Amulets");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        //Need Life Roll
        Affix.add("# to maximum Life:60:0");
        // Any 3 other
        Affix.add("#% to Fire Resistance:36");
        Affix.add("#% to Lightning Resistance:36");
        Affix.add("#% to Cold Resistance:36");
        Affix.add("#% to Chaos Resistance:21");
        Affix.add("# to Strength:33");
        Affix.add("# to Dexterity:33");
        Affix.add("# to Intelligence:33");
        Affix.add("# to Accuracy Rating:166");
        Affix.add("#% increased Elemental Damage with Attack Skills:21");
        Affix.add("Adds # to # Fire Damage to Attacks:24");
        Affix.add("Adds # to # Cold Damage to Attacks:21");
        Affix.add("Adds # to # Lightning Damage to Attacks:30");
        Affix.add("Adds # to # Physical Damage to Attacks:11");
        Affix.add("#% to all Elemental Resistances:12");
        Affix.add("# to all Attributes:25");
        Affix.add("#% increased Global Critical Strike Chance:25");
        Affix.add("#% to Global Critical Strike Multiplier:25");
        Affix.add("#% increased maximum Energy Shield:17");
        Affix.add("#% increased Evasion Rating:17");
        Affix.add("#% increased Armour:17");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //AMULETS ATTACK 4 MOD ES
        Prop = new ArrayList();
        Prop.add("ItemClass:Amulets");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        //Need ES Roll
        Affix.add("# to maximum Energy Shield:38:0");
        // Any 3 other
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("# to Strength:33");
        Affix.add("# to Dexterity:33");
        Affix.add("# to Intelligence:33");
        Affix.add("# to Accuracy Rating:251");
        Affix.add("#% increased Elemental Damage with Attack Skills:31");
        Affix.add("Adds # to # Fire Damage to Attacks:24");
        Affix.add("Adds # to # Cold Damage to Attacks:21");
        Affix.add("Adds # to # Lightning Damage to Attacks:30");
        Affix.add("Adds # to # Physical Damage to Attacks:11");
        Affix.add("#% to all Elemental Resistances:15");
        Affix.add("# to all Attributes:21");
        Affix.add("#% increased Global Critical Strike Chance:30");
        Affix.add("#% to Global Critical Strike Multiplier:30");
        Affix.add("#% increased maximum Energy Shield:20");
        Affix.add("#% increased Evasion Rating:20");
        Affix.add("#% increased Armour:20");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //AMULETS CASTER 3 MOD LIFE
        Prop = new ArrayList();
        Prop.add("ItemClass:Amulets");
        Affix = new ArrayList();
        //Life Roll
        Affix.add("# to maximum Life:70:0");
        //Any 2
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("#% to all Elemental Resistances:15");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        Affix.add("# to all Attributes:25");
        Affix.add("#% increased Cast Speed:8");
        Affix.add("# to maximum Mana:65");
        Affix.add("#% increased Mana Regeneration Rate:50");
        Affix.add("#% increased Spell Damage:18");
        Affix.add("#% increased Fire Damage:18");
        Affix.add("#% increased Cold Damage:18");
        Affix.add("#% increased Lightning Damage:18");
        Affix.add("#% increased Global Critical Strike Chance:30");
        Affix.add("#% to Global Critical Strike Multiplier:30");
        Affix.add("#% increased maximum Energy Shield:17");
        
        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //AMULETS CASTER 3 MOD ES
        Prop = new ArrayList();
        Prop.add("ItemClass:Amulets");
        Affix = new ArrayList();
        //Roll ES
        Affix.add("# to maximum Energy Shield:44:0");
        //Any 2
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("#% to all Elemental Resistances:15");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        Affix.add("# to all Attributes:25");
        Affix.add("#% increased Cast Speed:8");
        Affix.add("# to maximum Mana:65");
        Affix.add("#% increased Mana Regeneration Rate:50");
        Affix.add("#% increased Spell Damage:18");
        Affix.add("#% increased Fire Damage:18");
        Affix.add("#% increased Cold Damage:18");
        Affix.add("#% increased Lightning Damage:18");
        Affix.add("#% increased Global Critical Strike Chance:30");
        Affix.add("#% to Global Critical Strike Multiplier:30");
        Affix.add("#% increased maximum Energy Shield:17");
        
        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
       
        //AMULETS CASTER 4 MOD LIFE
        Prop = new ArrayList();
        Prop.add("ItemClass:Amulets");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        //ROll Life
        Affix.add("# to maximum Life:60:0");
        //Any 3
        Affix.add("#% to Fire Resistance:36");
        Affix.add("#% to Lightning Resistance:36");
        Affix.add("#% to Cold Resistance:36");
        Affix.add("#% to Chaos Resistance:21");
        Affix.add("# to Strength:33");
        Affix.add("# to Dexterity:33");
        Affix.add("# to Intelligence:33");
        Affix.add("#% to all Elemental Resistances:12");
        Affix.add("# to all Attributes:21");
        Affix.add("#% increased Cast Speed:8");
        Affix.add("# to maximum Mana:60");
        Affix.add("#% increased Mana Regeneration Rate:40");
        Affix.add("#% increased Spell Damage:13");
        Affix.add("#% increased Fire Damage:13");
        Affix.add("#% increased Cold Damage:13");
        Affix.add("#% increased Lightning Damage:13");
        Affix.add("#% increased Global Critical Strike Chance:25");
        Affix.add("#% to Global Critical Strike Multiplier:25");
        Affix.add("#% increased maximum Energy Shield:14");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //AMULETS CASTER 3 MOD ES
        Prop = new ArrayList();
        Prop.add("ItemClass:Amulets");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        //ROll ES
        Affix.add("# to maximum Energy Shield:38:0");
        //Any 3
        Affix.add("#% to Fire Resistance:36");
        Affix.add("#% to Lightning Resistance:36");
        Affix.add("#% to Cold Resistance:36");
        Affix.add("#% to Chaos Resistance:21");
        Affix.add("# to Strength:33");
        Affix.add("# to Dexterity:33");
        Affix.add("# to Intelligence:33");
        Affix.add("#% to all Elemental Resistances:12");
        Affix.add("# to all Attributes:21");
        Affix.add("#% increased Cast Speed:8");
        Affix.add("# to maximum Mana:60");
        Affix.add("#% increased Mana Regeneration Rate:40");
        Affix.add("#% increased Spell Damage:13");
        Affix.add("#% increased Fire Damage:13");
        Affix.add("#% increased Cold Damage:13");
        Affix.add("#% increased Lightning Damage:13");
        Affix.add("#% increased Global Critical Strike Chance:25");
        Affix.add("#% to Global Critical Strike Multiplier:25");
        Affix.add("#% increased maximum Energy Shield:14");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        
        //RINGS ATTACKS 2 MOD LIFE
        Prop = new ArrayList();
        Prop.add("ItemClass:Rings");
        Affix = new ArrayList();
        //Life Roll
        Affix.add("# to maximum Life:60:0");
        //Any 2
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("#% to all Elemental Resistances:12");
        Affix.add("# to all Attributes:13");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        Affix.add("#% increased Fire Damage:18");
        Affix.add("#% increased Cold Damage:18");
        Affix.add("# to Accuracy Rating:251");
        Affix.add("#% increased Elemental Damage with Attack Skills:31");
        Affix.add("Adds # to # Fire Damage to Attacks:24");
        Affix.add("Adds # to # Cold Damage to Attacks:21");
        Affix.add("Adds # to # Lightning Damage to Attacks:30");
        Affix.add("Adds # to # Physical Damage to Attacks:8");
        Affix.add("#% increased Attack Speed:5");
        Affix.add("# to Level of Socketed Gems:2");
        Affix.add("#% of Physical Attack Damage Leeched as Mana:0.2");
        Affix.add("#% of Physical Attack Damage Leeched as Life:0.2");

        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //RINGS ATTACKS 2 MOD ES
        Prop = new ArrayList();
        Prop.add("ItemClass:Rings");
        Affix = new ArrayList();
        //Roll ES
        Affix.add("# to maximum Energy Shield:44:0");
        //Any 2
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("#% to all Elemental Resistances:12");
        Affix.add("# to all Attributes:13");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        Affix.add("#% increased Fire Damage:18");
        Affix.add("#% increased Cold Damage:18");
        Affix.add("#% increased Lightning Damage:18"); 
        Affix.add("# to Accuracy Rating:251");
        Affix.add("#% increased Elemental Damage with Attack Skills:31");
        Affix.add("Adds # to # Fire Damage to Attacks:24");
        Affix.add("Adds # to # Cold Damage to Attacks:21");
        Affix.add("Adds # to # Lightning Damage to Attacks:30");
        Affix.add("Adds # to # Physical Damage to Attacks:8");
        Affix.add("#% increased Attack Speed:5");
        Affix.add("# to Level of Socketed Gems:2");
        Affix.add("#% of Physical Attack Damage Leeched as Mana:0.2");
        Affix.add("#% of Physical Attack Damage Leeched as Life:0.2");

        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //RINGS ATTACKS 3 MOD LIFE
        Prop = new ArrayList();
        Prop.add("ItemClass:Rings");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        //Roll Life
        Affix.add("# to maximum Life:50:0");
        //Any 3
        Affix.add("#% to Fire Resistance:36");
        Affix.add("#% to Lightning Resistance:36");
        Affix.add("#% to Cold Resistance:36");
        Affix.add("#% to Chaos Resistance:21");
        Affix.add("#% to all Elemental Resistances:9");
        Affix.add("# to all Attributes:9");
        Affix.add("# to Strength:33");
        Affix.add("# to Dexterity:33");
        Affix.add("# to Intelligence:33");
        Affix.add("#% increased Fire Damage:13");
        Affix.add("#% increased Cold Damage:13");
        Affix.add("#% increased Lightning Damage:13");
        Affix.add("# to Accuracy Rating:166");
        Affix.add("#% increased Elemental Damage with Attack Skills:21");
        Affix.add("Adds # to # Fire Damage to Attacks:20");
        Affix.add("Adds # to # Cold Damage to Attacks:20");
        Affix.add("Adds # to # Lightning Damage to Attacks:24");
        Affix.add("Adds # to # Physical Damage to Attacks:6");
        Affix.add("#% increased Attack Speed:5");
        Affix.add("# to Level of Socketed Gems:2");
        Affix.add("#% of Physical Attack Damage Leeched as Mana:0.2");
        Affix.add("#% of Physical Attack Damage Leeched as Life:0.2");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //RINGS ATTACKS 3 MOD ES
        Prop = new ArrayList();
        Prop.add("ItemClass:Rings");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        //Roll ES
        Affix.add("# to maximum Energy Shield:38:0");
        // Any 3
        Affix.add("#% to Fire Resistance:36");
        Affix.add("#% to Lightning Resistance:36");
        Affix.add("#% to Cold Resistance:36");
        Affix.add("#% to Chaos Resistance:21");
        Affix.add("#% to all Elemental Resistances:9");
        Affix.add("# to all Attributes:9");
        Affix.add("# to Strength:33");
        Affix.add("# to Dexterity:33");
        Affix.add("# to Intelligence:33");
        Affix.add("#% increased Fire Damage:13");
        Affix.add("#% increased Cold Damage:13");
        Affix.add("#% increased Lightning Damage:13");
        Affix.add("# to Accuracy Rating:166");
        Affix.add("#% increased Elemental Damage with Attack Skills:21");
        Affix.add("Adds # to # Fire Damage to Attacks:20");
        Affix.add("Adds # to # Cold Damage to Attacks:20");
        Affix.add("Adds # to # Lightning Damage to Attacks:24");
        Affix.add("Adds # to # Physical Damage to Attacks:6");   
        Affix.add("#% increased Attack Speed:5");
        Affix.add("# to Level of Socketed Gems:2");
        Affix.add("#% of Physical Attack Damage Leeched as Mana:0.2");
        Affix.add("#% of Physical Attack Damage Leeched as Life:0.2");

        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        
        //RINGS CASTER 2 MOD LIFE
        Prop = new ArrayList();
        Prop.add("ItemClass:Rings");
        Affix = new ArrayList();
        //Roll Life
        Affix.add("# to maximum Life:60:0");
        //Any 2
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("#% to all Elemental Resistances:12");
        Affix.add("# to all Attributes:13");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        Affix.add("#% increased Fire Damage:18");
        Affix.add("#% increased Cold Damage:18");
        Affix.add("#% increased Lightning Damage:18");
        Affix.add("#% increased Attack Speed:5");
        Affix.add("#% increased Cast Speed:5");
        Affix.add("# to maximum Mana:69");
        Affix.add("#% increased Mana Regeneration Rate:50");
        Affix.add("# to Level of Socketed Gems:2");
        
        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //RINGS CASTER 2 MOD ES
        Prop = new ArrayList();
        Prop.add("ItemClass:Rings");
        Affix = new ArrayList();
        //Roll ES
        Affix.add("# to maximum Energy Shield:44:0");
        //Any 2
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("#% to all Elemental Resistances:12");
        Affix.add("# to all Attributes:13");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        Affix.add("#% increased Fire Damage:18");
        Affix.add("#% increased Cold Damage:18");
        Affix.add("#% increased Lightning Damage:18");
        Affix.add("#% increased Attack Speed:5");
        Affix.add("#% increased Cast Speed:5");
        Affix.add("# to maximum Mana:69");
        Affix.add("#% increased Mana Regeneration Rate:50");
        Affix.add("# to Level of Socketed Gems:2");
        
        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //RINGS CASTER 3 MOD LIFE
        Prop = new ArrayList();
        Prop.add("ItemClass:Rings");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        //Roll Life
        Affix.add("# to maximum Life:50:0");
        //Any 3
        Affix.add("#% to Fire Resistance:36");
        Affix.add("#% to Lightning Resistance:36");
        Affix.add("#% to Cold Resistance:36");
        Affix.add("#% to Chaos Resistance:21");
        Affix.add("#% to all Elemental Resistances:9");
        Affix.add("# to all Attributes:9");
        Affix.add("# to Strength:33");
        Affix.add("# to Dexterity:33");
        Affix.add("# to Intelligence:33");
        Affix.add("#% increased Fire Damage:13");
        Affix.add("#% increased Cold Damage:13");
        Affix.add("#% increased Lightning Damage:13");
        Affix.add("#% increased Attack Speed:5");
        Affix.add("#% increased Cast Speed:5");
        Affix.add("# to maximum Mana:69");
        Affix.add("#% increased Mana Regeneration Rate:50");
        Affix.add("# to Level of Socketed Gems:2");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
     
        
        //RINGS CASTER 3 MOD ES
        Prop = new ArrayList();
        Prop.add("ItemClass:Rings");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        //Roll ES
        Affix.add("# to maximum Energy Shield:32:0");
        //Any 3
        Affix.add("#% to Fire Resistance:36");
        Affix.add("#% to Lightning Resistance:36");
        Affix.add("#% to Cold Resistance:36");
        Affix.add("#% to Chaos Resistance:21");
        Affix.add("#% to all Elemental Resistances:9");
        Affix.add("# to all Attributes:9");
        Affix.add("# to Strength:33");
        Affix.add("# to Dexterity:33");
        Affix.add("# to Intelligence:33");
        Affix.add("#% increased Fire Damage:13");
        Affix.add("#% increased Cold Damage:13");
        Affix.add("#% increased Lightning Damage:13");
        Affix.add("#% increased Attack Speed:5");
        Affix.add("#% increased Cast Speed:5");
        Affix.add("# to maximum Mana:65");
        Affix.add("#% increased Mana Regeneration Rate:40");  
        Affix.add("# to Level of Socketed Gems:2");
 
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //BELTS 4 MOD VEILED
        Prop = new ArrayList();
        Prop.add("ItemClass:Belts");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        Affix.add("# to maximum Life:80");
        Affix.add("# to maximum Energy Shield:44");

        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        
        Affix.add("#% increased Flask Effect Duration:10");
        Affix.add("#% reduced Flask Charges used:10");
        Affix.add("#% increased Flask Charges gained:21");
        
        Affix.add("# to Armour:401");
        Affix.add("#% increased Elemental Damage with Attack Skills:37");
        Affix.add("# to maximum Mana:60");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //BELTS 4 MOD LIFE
        Prop = new ArrayList();
        Prop.add("ItemClass:Belts");
        Affix = new ArrayList();
        Affix.add("# to maximum Life:80:0");

        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        
        Affix.add("#% increased Flask Effect Duration:10");
        Affix.add("#% reduced Flask Charges used:10");
        Affix.add("#% increased Flask Charges gained:21");
        
        Affix.add("# to Armour:401");
        Affix.add("#% increased Elemental Damage with Attack Skills:37");
        Affix.add("# to maximum Mana:60");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);

        //BELTS 4 MOD ES
        Prop = new ArrayList();
        Prop.add("ItemClass:Belts");
        Affix = new ArrayList();
        Affix.add("# to maximum Energy Shield:44:0");

        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        
        Affix.add("#% increased Flask Effect Duration:10");
        Affix.add("#% reduced Flask Charges used:10");
        Affix.add("#% increased Flask Charges gained:21");
        
        Affix.add("# to Armour:401");
        Affix.add("#% increased Elemental Damage with Attack Skills:37");
        Affix.add("# to maximum Mana:60");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        
        //BELTS 5 MOD VEILED
        Prop = new ArrayList();
        Prop.add("ItemClass:Belts");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        Affix.add("# to maximum Life:70");
        Affix.add("# to maximum Energy Shield:38");

        Affix.add("#% to Fire Resistance:36");
        Affix.add("#% to Lightning Resistance:36");
        Affix.add("#% to Cold Resistance:36");
        Affix.add("#% to Chaos Resistance:21");
        
        Affix.add("# to Strength:33");
        Affix.add("# to Dexterity:33");
        Affix.add("# to Intelligence:33");
        
        Affix.add("#% increased Flask Effect Duration:10");
        Affix.add("#% reduced Flask Charges used:10");
        Affix.add("#% increased Flask Charges gained:10");
        
        Affix.add("# to Armour:323");
        Affix.add("#% increased Elemental Damage with Attack Skills:31");
        Affix.add("# to maximum Mana:55");
        
        OrCount = 4;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //BELTS 5 MOD LIFE
        Prop = new ArrayList();
        Prop.add("ItemClass:Belts");
        Affix = new ArrayList();
        Affix.add("# to maximum Life:70:0");

        Affix.add("#% to Fire Resistance:36");
        Affix.add("#% to Lightning Resistance:36");
        Affix.add("#% to Cold Resistance:36");
        Affix.add("#% to Chaos Resistance:21");
        
        Affix.add("# to Strength:33");
        Affix.add("# to Dexterity:33");
        Affix.add("# to Intelligence:33");
        
        Affix.add("#% increased Flask Effect Duration:10");
        Affix.add("#% reduced Flask Charges used:10");
        Affix.add("#% increased Flask Charges gained:10");
        
        Affix.add("# to Armour:323");
        Affix.add("#% increased Elemental Damage with Attack Skills:31");
        Affix.add("# to maximum Mana:55");
        
        OrCount = 4;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);

        //BELTS 4 MOD ES
        Prop = new ArrayList();
        Prop.add("ItemClass:Belts");
        Affix = new ArrayList();
        Affix.add("# to maximum Energy Shield:38:0");

        Affix.add("#% to Fire Resistance:36");
        Affix.add("#% to Lightning Resistance:36");
        Affix.add("#% to Cold Resistance:36");
        Affix.add("#% to Chaos Resistance:21");
        
        Affix.add("# to Strength:33");
        Affix.add("# to Dexterity:33");
        Affix.add("# to Intelligence:33");
        
        Affix.add("#% increased Flask Effect Duration:10");
        Affix.add("#% reduced Flask Charges used:10");
        Affix.add("#% increased Flask Charges gained:10");
        
        Affix.add("# to Armour:323");
        Affix.add("#% increased Elemental Damage with Attack Skills:31");
        Affix.add("# to maximum Mana:55");
        
        OrCount = 4;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        

        //QUIVERS 4 MOD
        Prop = new ArrayList();
        Prop.add("ItemClass:Quivers");
        Affix = new ArrayList();
        Affix.add("# to maximum Life:80:0");
        
        Affix.add("# to Accuracy Rating:251");

        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        
        Affix.add("# to Dexterity:38");
        Affix.add("#% increased Attack Speed:8");
        Affix.add("#% increased Global Critical Strike Chance:30");
        Affix.add("#% to Global Critical Strike Multiplier:25");
        Affix.add("#% to Damage over Time Multiplier:12");
        Affix.add("#% increased Projectile Speed:34");
        
        Affix.add("Adds # to # Physical Damage to Attacks:6");
        Affix.add("Adds # to # Fire Damage to Attacks:11");
        Affix.add("Adds # to # Cold Damage to Attacks:9");
        Affix.add("Adds # to # Lightning Damage to Attacks:14");
        
        Affix.add("#% of Physical Attack Damage Leeched as Mana:0.2");
        Affix.add("#% of Physical Attack Damage Leeched as Life:0.2");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //QUIVERS 5 MOD
        Prop = new ArrayList();
        Prop.add("ItemClass:Quivers");
        Affix = new ArrayList();
        Affix.add("# to maximum Life:60:0");
        
        Affix.add("# to Accuracy Rating:166");

        Affix.add("#% to Fire Resistance:36");
        Affix.add("#% to Lightning Resistance:36");
        Affix.add("#% to Cold Resistance:36");
        Affix.add("#% to Chaos Resistance:21");
        
        Affix.add("# to Dexterity:33");
        Affix.add("#% increased Attack Speed:5");
        Affix.add("#% increased Global Critical Strike Chance:25");
        Affix.add("#% to Global Critical Strike Multiplier:20");
        Affix.add("#% to Damage over Time Multiplier:7");
        Affix.add("#% increased Projectile Speed:26");
        
        Affix.add("Adds # to # Physical Damage to Attacks:6");
        Affix.add("Adds # to # Fire Damage to Attacks:11");
        Affix.add("Adds # to # Cold Damage to Attacks:9");
        Affix.add("Adds # to # Lightning Damage to Attacks:14");
        
        Affix.add("#% of Physical Attack Damage Leeched as Mana:0.2");
        Affix.add("#% of Physical Attack Damage Leeched as Life:0.2");
        
        OrCount = 4;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //SHIELDS LIFE CASTER 2 LIFE
        
        Prop = new ArrayList();
        Prop.add("ItemClass:Shields");
        Affix = new ArrayList();
        //Life Roll
        Affix.add("# to maximum Life:90:0");
        //Any 2
        Affix.add("# to maximum Mana:65");
        Affix.add("#% increased Mana Regeneration Rate:50");
        Affix.add("#% increased Critical Strike Chance for Spells:80");
        Affix.add("#% increased Cast Speed:11");
        Affix.add("Gain #% of Fire Damage as Extra Chaos Damage:11");
        Affix.add("Gain #% of Cold Damage as Extra Chaos Damage:11");
        Affix.add("Gain #% of Lightning Damage as Extra Chaos Damage:11");
        Affix.add("Gain #% of Physical Damage as Extra Chaos Damage:11");
        Affix.add("# to Level of all Physical Spell Skill Gems:1");
        Affix.add("# to Level of all Fire Spell Skill Gems:1");
        Affix.add("# to Level of all Cold Spell Skill Gems:1");
        Affix.add("# to Level of all Lightning Spell Skill Gems:1");
        Affix.add("# to Level of all Chaos Spell Skill Gems:1");
        Affix.add("#% increased Lightning Damage:85");
        Affix.add("#% increased Cold Damage:85");
        Affix.add("#% increased Fire Damage:85");
        Affix.add("#% increased Spell Damage:65");
        Affix.add("#% Chance to Block:7");
        Affix.add("#% Chance to Block Spell Damage:10");
        Affix.add("#% Chance to Block Projectile Attack Damage:11");
        Affix.add("# Life gained when you Block:86");
        Affix.add("#% to all Elemental Resistances:15");
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("# to Strength:43 ");
        Affix.add("# to Dexterity:43");
        Affix.add("# to Intelligence:43");
        
        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //SHIELDS LIFE CASTER 2 ES 1
        Prop = new ArrayList();
        Prop.add("ItemClass:Shields");
        Affix = new ArrayList();
        //ES roll
        Affix.add("# to maximum Energy Shield:62:0");
        //ANy 2
        Affix.add("# to maximum Mana:65");
        Affix.add("#% increased Mana Regeneration Rate:50");
        Affix.add("#% increased Critical Strike Chance for Spells:80");
        Affix.add("#% increased Cast Speed:11");
        Affix.add("Gain #% of Fire Damage as Extra Chaos Damage:11");
        Affix.add("Gain #% of Cold Damage as Extra Chaos Damage:11");
        Affix.add("Gain #% of Lightning Damage as Extra Chaos Damage:11");
        Affix.add("Gain #% of Physical Damage as Extra Chaos Damage:11");
        Affix.add("# to Level of all Physical Spell Skill Gems:1");
        Affix.add("# to Level of all Fire Spell Skill Gems:1");
        Affix.add("# to Level of all Cold Spell Skill Gems:1");
        Affix.add("# to Level of all Lightning Spell Skill Gems:1");
        Affix.add("# to Level of all Chaos Spell Skill Gems:1");
        Affix.add("#% increased Lightning Damage:85");
        Affix.add("#% increased Cold Damage:85");
        Affix.add("#% increased Fire Damage:85");
        Affix.add("#% increased Spell Damage:65");
        Affix.add("#% Chance to Block:7");
        Affix.add("#% Chance to Block Spell Damage:10");
        Affix.add("#% Chance to Block Projectile Attack Damage:11");
        Affix.add("# Life gained when you Block:86");
        Affix.add("#% to all Elemental Resistances:15");
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("# to Strength:43 ");
        Affix.add("# to Dexterity:43");
        Affix.add("# to Intelligence:43");
        
        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //SHIELDS LIFE CASTER 2 MODS ES 2
        Prop = new ArrayList();
        Prop.add("ItemClass:Shields");
        Affix = new ArrayList();
        //Es Roll
        Affix.add("#% increased Energy Shield:92:0");
        //Any 2
        Affix.add("# to maximum Mana:65");
        Affix.add("#% increased Mana Regeneration Rate:50");
        Affix.add("#% increased Critical Strike Chance for Spells:80");
        Affix.add("#% increased Cast Speed:11");
        Affix.add("Gain #% of Fire Damage as Extra Chaos Damage:11");
        Affix.add("Gain #% of Cold Damage as Extra Chaos Damage:11");
        Affix.add("Gain #% of Lightning Damage as Extra Chaos Damage:11");
        Affix.add("Gain #% of Physical Damage as Extra Chaos Damage:11");
        Affix.add("# to Level of all Physical Spell Skill Gems:1");
        Affix.add("# to Level of all Fire Spell Skill Gems:1");
        Affix.add("# to Level of all Cold Spell Skill Gems:1");
        Affix.add("# to Level of all Lightning Spell Skill Gems:1");
        Affix.add("# to Level of all Chaos Spell Skill Gems:1");
        Affix.add("#% increased Lightning Damage:85");
        Affix.add("#% increased Cold Damage:85");
        Affix.add("#% increased Fire Damage:85");
        Affix.add("#% increased Spell Damage:65");
        Affix.add("#% Chance to Block:7");
        Affix.add("#% Chance to Block Spell Damage:10");
        Affix.add("#% Chance to Block Projectile Attack Damage:11");
        Affix.add("# Life gained when you Block:86");
        Affix.add("#% to all Elemental Resistances:15");
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("# to Strength:43 ");
        Affix.add("# to Dexterity:43");
        Affix.add("# to Intelligence:43");
        
        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        
        //SHIELDS LIFE CASTER 3 LIFE
        Prop = new ArrayList();
        Prop.add("ItemClass:Shields");
        Affix = new ArrayList();
        Affix.add("# to maximum Life:80:0");

        Affix.add("# to maximum Mana:60");
        Affix.add("#% increased Mana Regeneration Rate:40");
        Affix.add("#% increased Critical Strike Chance for Spells:60");
        Affix.add("#% increased Cast Speed:8");
        Affix.add("Gain #% of Fire Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Cold Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Lightning Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Physical Damage as Extra Chaos Damage:8");
        Affix.add("# to Level of all Physical Spell Skill Gems:1");
        Affix.add("# to Level of all Fire Spell Skill Gems:1");
        Affix.add("# to Level of all Cold Spell Skill Gems:1");
        Affix.add("# to Level of all Lightning Spell Skill Gems:1");
        Affix.add("# to Level of all Chaos Spell Skill Gems:1");
        Affix.add("#% increased Lightning Damage:65");
        Affix.add("#% increased Cold Damage:65");
        Affix.add("#% increased Fire Damage:65");
        Affix.add("#% increased Spell Damage:50");
        Affix.add("#% Chance to Block:7");
        Affix.add("#% Chance to Block Spell Damage:10");
        Affix.add("#% Chance to Block Projectile Attack Damage:11");
        Affix.add("# Life gained when you Block:86");
        Affix.add("#% to all Elemental Resistances:12");
        Affix.add("#% to Fire Resistance:36");
        Affix.add("#% to Lightning Resistance:36");
        Affix.add("#% to Cold Resistance:36");
        Affix.add("#% to Chaos Resistance:21");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        

        //SHIELDS LIFE CASTER 3 ES 1
        Prop = new ArrayList();
        Prop.add("ItemClass:Shields");
        Affix = new ArrayList();
        Affix.add("# to maximum Energy Shield:50:0");

        Affix.add("# to maximum Mana:60");
        Affix.add("#% increased Mana Regeneration Rate:40");
        Affix.add("#% increased Critical Strike Chance for Spells:60");
        Affix.add("#% increased Cast Speed:8");
        Affix.add("Gain #% of Fire Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Cold Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Lightning Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Physical Damage as Extra Chaos Damage:8");
        Affix.add("# to Level of all Physical Spell Skill Gems:1");
        Affix.add("# to Level of all Fire Spell Skill Gems:1");
        Affix.add("# to Level of all Cold Spell Skill Gems:1");
        Affix.add("# to Level of all Lightning Spell Skill Gems:1");
        Affix.add("# to Level of all Chaos Spell Skill Gems:1");
        Affix.add("#% increased Lightning Damage:65");
        Affix.add("#% increased Cold Damage:65");
        Affix.add("#% increased Fire Damage:65");
        Affix.add("#% increased Spell Damage:50");
        Affix.add("#% Chance to Block:7");
        Affix.add("#% Chance to Block Spell Damage:10");
        Affix.add("#% Chance to Block Projectile Attack Damage:11");
        Affix.add("# Life gained when you Block:86");
        Affix.add("#% to all Elemental Resistances:12");
        Affix.add("#% to Fire Resistance:36");
        Affix.add("#% to Lightning Resistance:36");
        Affix.add("#% to Cold Resistance:36");
        Affix.add("#% to Chaos Resistance:21");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //SHIELDS LIFE CASTER ES 2
        Prop = new ArrayList();
        Prop.add("ItemClass:Shields");
        Affix = new ArrayList();
        Affix.add("#% increased Energy Shield:80:0");

        Affix.add("# to maximum Mana:60");
        Affix.add("#% increased Mana Regeneration Rate:40");
        Affix.add("#% increased Critical Strike Chance for Spells:60");
        Affix.add("#% increased Cast Speed:8");
        Affix.add("Gain #% of Fire Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Cold Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Lightning Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Physical Damage as Extra Chaos Damage:8");
        Affix.add("# to Level of all Physical Spell Skill Gems:1");
        Affix.add("# to Level of all Fire Spell Skill Gems:1");
        Affix.add("# to Level of all Cold Spell Skill Gems:1");
        Affix.add("# to Level of all Lightning Spell Skill Gems:1");
        Affix.add("# to Level of all Chaos Spell Skill Gems:1");
        Affix.add("#% increased Lightning Damage:65");
        Affix.add("#% increased Cold Damage:65");
        Affix.add("#% increased Fire Damage:65");
        Affix.add("#% increased Spell Damage:50");
        Affix.add("#% Chance to Block:7");
        Affix.add("#% Chance to Block Spell Damage:10");
        Affix.add("#% Chance to Block Projectile Attack Damage:11");
        Affix.add("# Life gained when you Block:86");
        Affix.add("#% to all Elemental Resistances:12");
        Affix.add("#% to Fire Resistance:36");
        Affix.add("#% to Lightning Resistance:36");
        Affix.add("#% to Cold Resistance:36");
        Affix.add("#% to Chaos Resistance:21");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        

        //SHIELDS ES CASTER 4 ES 1
        Prop = new ArrayList();
        Prop.add("ItemClass:Shields");
        Affix = new ArrayList();
        Affix.add("# to maximum Energy Shield:39:0");

        Affix.add("#% increased Lightning Damage:65");
        Affix.add("#% increased Cold Damage:65");
        Affix.add("#% increased Fire Damage:65");
        Affix.add("#% increased Spell Damage:50");    
        Affix.add("# to Level of all Physical Spell Skill Gems:1");
        Affix.add("# to Level of all Fire Spell Skill Gems:1");
        Affix.add("# to Level of all Cold Spell Skill Gems:1");
        Affix.add("# to Level of all Lightning Spell Skill Gems:1");
        Affix.add("# to Level of all Chaos Spell Skill Gems:1");
        Affix.add("Gain #% of Fire Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Cold Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Lightning Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Physical Damage as Extra Chaos Damage:8");
        Affix.add("#% increased Cast Speed:8");
        Affix.add("#% increased Critical Strike Chance for Spells:60");    
        Affix.add("#% increased Mana Regeneration Rate:40");
        Affix.add("# to maximum Mana:60");
        Affix.add("#% Chance to Block:7");
        Affix.add("#% Chance to Block Spell Damage:10");
        Affix.add("#% Chance to Block Projectile Attack Damage:11");
        Affix.add("# Life gained when you Block:86");
        Affix.add("#% to all Elemental Resistances:15");
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        
        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //SHIELDS ES CASTER 4 ES 2
        Prop = new ArrayList();
        Prop.add("ItemClass:Shields");
        Affix = new ArrayList();
        Affix.add("#% increased Energy Shield:68:0");

        Affix.add("#% increased Lightning Damage:65");
        Affix.add("#% increased Cold Damage:65");
        Affix.add("#% increased Fire Damage:65");
        Affix.add("#% increased Spell Damage:50");    
        Affix.add("# to Level of all Physical Spell Skill Gems:1");
        Affix.add("# to Level of all Fire Spell Skill Gems:1");
        Affix.add("# to Level of all Cold Spell Skill Gems:1");
        Affix.add("# to Level of all Lightning Spell Skill Gems:1");
        Affix.add("# to Level of all Chaos Spell Skill Gems:1");
        Affix.add("Gain #% of Fire Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Cold Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Lightning Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Physical Damage as Extra Chaos Damage:8");
        Affix.add("#% increased Cast Speed:8");
        Affix.add("#% increased Critical Strike Chance for Spells:60");    
        Affix.add("#% increased Mana Regeneration Rate:40");
        Affix.add("# to maximum Mana:60");
        Affix.add("#% Chance to Block:7");
        Affix.add("#% Chance to Block Spell Damage:10");
        Affix.add("#% Chance to Block Projectile Attack Damage:11");
        Affix.add("# Life gained when you Block:86");
        Affix.add("#% to all Elemental Resistances:15");
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        
        OrCount  = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        
        //SHIELDS ES CASTER 2 
        Prop = new ArrayList();
        Prop.add("ItemClass:Shields");
        Affix = new ArrayList();
        //Roll both ES mods
        Affix.add("#% increased Energy Shield:80:0");
        Affix.add("# to maximum Energy Shield:50:0");
        // Any Other Mod
        Affix.add("#% increased Lightning Damage:85");
        Affix.add("#% increased Cold Damage:85");
        Affix.add("#% increased Fire Damage:85");
        Affix.add("#% increased Spell Damage:65");    
        Affix.add("# to Level of all Physical Spell Skill Gems:1");
        Affix.add("# to Level of all Fire Spell Skill Gems:1");
        Affix.add("# to Level of all Cold Spell Skill Gems:1");
        Affix.add("# to Level of all Lightning Spell Skill Gems:1");
        Affix.add("# to Level of all Chaos Spell Skill Gems:1");
        Affix.add("Gain #% of Fire Damage as Extra Chaos Damage:11");
        Affix.add("Gain #% of Cold Damage as Extra Chaos Damage:11");
        Affix.add("Gain #% of Lightning Damage as Extra Chaos Damage:11");
        Affix.add("Gain #% of Physical Damage as Extra Chaos Damage:11");
        Affix.add("#% increased Cast Speed:11");
        Affix.add("#% increased Critical Strike Chance for Spells:80");    
        Affix.add("#% increased Mana Regeneration Rate:50");
        Affix.add("# to maximum Mana:65");
        Affix.add("#% Chance to Block:7");
        Affix.add("#% Chance to Block Spell Damage:10");
        Affix.add("#% Chance to Block Projectile Attack Damage:11");
        Affix.add("# Life gained when you Block:86");
        Affix.add("#% to all Elemental Resistances:15");
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        
        OrCount  = 1;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        

        //SHIELDS DAMAGE CASTER 3 MODS
        Prop = new ArrayList();
        Prop.add("ItemClass:Shields");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        Affix.add("#% increased Spell Damage:65");
        
        Affix.add("#% increased Fire Damage:65");
        Affix.add("#% increased Cold Damage:65");
        Affix.add("#% increased Lightning Damage:65");
        
        Affix.add("# to Level of all Cold Spell Skill Gems:1");
        Affix.add("# to Level of all Fire Spell Skill Gems:1");
        Affix.add("# to Level of all Lightning Spell Skill Gems:1");
        Affix.add("# to Level of all Physical Spell Skill Gems:1");
        Affix.add("# to Level of all Chaos Spell Skill Gems:1");
        
        Affix.add("Gain #% of Physical Damage as Extra Chaos Damage:11");
        Affix.add("Gain #% of Lightning Damage as Extra Chaos Damage:11");
        Affix.add("Gain #% of Cold Damage as Extra Chaos Damage:11");
        Affix.add("Gain #% of Fire Damage as Extra Chaos Damage:11");
        
        Affix.add("#% increased Cast Speed:11");
        Affix.add("#% increased Critical Strike Chance for Spells:80");
        
        Affix.add("#% increased Mana Regeneration Rate:50");
        Affix.add("# to maximum Mana:65");
        
        OrCount =  3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //SHIELDS DAMAGE CASTER 4 MODS
        Prop = new ArrayList();
        Prop.add("ItemClass:Shields");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        Affix.add("#% increased Spell Damage:50");
        
        Affix.add("#% increased Fire Damage:50");
        Affix.add("#% increased Cold Damage:50");
        Affix.add("#% increased Lightning Damage:50");
        
        Affix.add("# to Level of all Cold Spell Skill Gems:1");
        Affix.add("# to Level of all Fire Spell Skill Gems:1");
        Affix.add("# to Level of all Lightning Spell Skill Gems:1");
        Affix.add("# to Level of all Physical Spell Skill Gems:1");
        Affix.add("# to Level of all Chaos Spell Skill Gems:1");
        
        Affix.add("Gain #% of Physical Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Lightning Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Cold Damage as Extra Chaos Damage:8");
        Affix.add("Gain #% of Fire Damage as Extra Chaos Damage:8");
        
        Affix.add("#% increased Cast Speed:8");
        Affix.add("#% increased Critical Strike Chance for Spells:60");
        
        Affix.add("% increased Mana Regeneration Rate:40");
        Affix.add("# to maximum Mana:60");
        
        Affix.add("#% Chance to Block:7");
        Affix.add("#% Chance to Block Spell Damage:10");
        Affix.add("#% Chance to Block Projectile Attack Damage:11");
        Affix.add("# Life gained when you Block:86");
       

        Affix.add("#% to all Elemental Resistances:15");
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        
        OrCount = 4;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        
        //SHIELDS DEF 2
        Prop = new ArrayList();
        Prop.add("ItemClass:Shields");
        Affix = new ArrayList();
        Affix.add("#% increased Armour:92");
        Affix.add("# to Armour:139");
        Affix.add("# to maximum Energy Shield:62");
        Affix.add("#% increased Energy Shield:92");
        Affix.add("#% increased Armour and Energy Shield:92");
        Affix.add("#% increased Evasion and Energy Shield:92");
        Affix.add("# to Evasion Rating:139");
        Affix.add("#% increased Evasion Rating:92");
        //to Evasion Rating, to maximum Energy Shield
        Affix.add("Spirit's:1");
        Affix.add("Eidolon's:1");
        //to Armour, to maximum Energy Shield
        Affix.add("Beatified:1");
        Affix.add("Consecrated:1");
        
        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //SHIELDS STATS
        Prop = new ArrayList();
        Prop.add("ItemClass:Shields");
        Affix = new ArrayList();
        Affix.add("# to maximum Life:80");
        Affix.add("#% increased Armour:92");
        Affix.add("# to Armour:139");
        Affix.add("# to maximum Energy Shield:62");
        Affix.add("#% increased Energy Shield:92");
        Affix.add("#% increased Armour and Energy Shield:92");
        Affix.add("#% increased Evasion and Energy Shield:92");
        Affix.add("# to Evasion Rating:139");
        Affix.add("#% increased Evasion Rating:92");
        
        Affix.add("#% Chance to Block:7");
        Affix.add("#% Chance to Block Spell Damage:10");
        Affix.add("#% Chance to Block Projectile Attack Damage:11");
        Affix.add("# Life gained when you Block:86");
        
        Affix.add("#% to all Elemental Resistances:15");
        Affix.add("#% to Fire Resistance:42");
        Affix.add("#% to Lightning Resistance:42");
        Affix.add("#% to Cold Resistance:42");
        Affix.add("#% to Chaos Resistance:26");
        
        Affix.add("# to Strength:38");
        Affix.add("# to Dexterity:38");
        Affix.add("# to Intelligence:38");
        
        //to Evasion Rating, to maximum Energy Shield
        Affix.add("Spirit's:1");
        Affix.add("Eidolon's:1");
        //to Armour, to maximum Energy Shield
        Affix.add("Beatified:1");
        Affix.add("Consecrated:1");
        
        OrCount = 4;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        
        //HELMETS LIFE 3 MOD
        Prop = new ArrayList();
        Prop.add("ItemClass:Helmets");
        Affix = new ArrayList();
        Affix.add("# to maximum Life:80:0");
        
        Affix.add("of Tzteosh:1");
        Affix.add("of the Magma:1");
        Affix.add("of Ephij:1");
        Affix.add("of the Lightning:1");
        Affix.add("of Haast:1");
        Affix.add("of the Ice:1");
        Affix.add("of Bameth:1");
        Affix.add("of Exile:1");
        
        Affix.add("of the Godslayer:1");
        Affix.add("of the Gods:1");
        Affix.add("of the Titan:1");
        Affix.add("of the Leviathan:1");
        Affix.add("of the Blur:1");
        Affix.add("of the Wind:1");
        Affix.add("of the Phantom:1");
        Affix.add("of the Jaguar:1");
        Affix.add("of the Polymath:1");
        Affix.add("of the Genius:1");
        Affix.add("of the Virtuoso:1");
        Affix.add("of the Savant:1");
        
        Affix.add("of Lioneye:1");
        Affix.add("of the Ranger:1");

        Affix.add("Necromancer's:1");
        Affix.add("Summoner's:1");
        
        Affix.add("Nautilus's:1");
        Affix.add("Urchin's:1");
        Affix.add("Ram's:1");
        Affix.add("Fawn's:1");
        Affix.add("Abbot's:1");
        Affix.add("Prior's:1");
        
        Affix.add("Blue:1");
        Affix.add("Zaffre:1");
        
        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);

        //HELMETS LIFE 4 MOD
        Prop = new ArrayList();
        Prop.add("ItemClass:Helmets");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        Affix.add("# to maximum Life:70:0");
        
        Affix.add("of Tzteosh:1");
        Affix.add("of the Magma:1");
        Affix.add("of the Volcano:1");
        Affix.add("of Ephij:1");
        Affix.add("of the Lightning:1");
        Affix.add("of the Maelstrom:1");
        Affix.add("of Haast:1");
        Affix.add("of the Ice:1");
        Affix.add("of the Polar Bear:1");
        Affix.add("of Bameth:1");
        Affix.add("of Exile:1");
        Affix.add("of Expulsion:1");
        Affix.add("of the Godslayer:1");
        Affix.add("of the Gods:1");
        Affix.add("of the Titan:1");
        Affix.add("of the Leviathan:1");
        Affix.add("of the Blur:1");
        Affix.add("of the Wind:1");
        Affix.add("of the Phantom:1");
        Affix.add("of the Jaguar:1");
        Affix.add("of the Polymath:1");
        Affix.add("of the Genius:1");
        Affix.add("of the Virtuoso:1");
        Affix.add("of the Savant:1");
        Affix.add("of Lioneye:1");
        Affix.add("of the Ranger:1");
        Affix.add("of the Marksman:1");
        Affix.add("Necromancer's:1");
        Affix.add("Summoner's:1");
        Affix.add("Nautilus's:1");
        Affix.add("Urchin's:1");
        Affix.add("Ram's:1");
        Affix.add("Fawn's:1");
        Affix.add("Abbot's:1");
        Affix.add("Prior's:1");
        Affix.add("Blue:1");
        Affix.add("Zaffre:1");
        Affix.add("Mazarine:1");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //HELMETS ES 3 MOD
        Prop = new ArrayList();
        Prop.add("ItemClass:Helmets");
        Affix = new ArrayList();
        Affix.add("Blazing:1");
        Affix.add("Seething:1");
        Affix.add("Pulsing:1");
        Affix.add("Unassailable:1");
        Affix.add("Indomitable:1");

        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //HELMETS ES 4 MOD
        Prop = new ArrayList();
        Prop.add("ItemClass:Helmets");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        
        Affix.add("Prior's:1");
        Affix.add("Deacon's:1");
        Affix.add("Seraphim's:1");
        Affix.add("Blazing:1");
        Affix.add("Seething:1");
        Affix.add("Pulsing:1");
        Affix.add("Unassailable:1");
        Affix.add("Indomitable:1");
        Affix.add("of Tzteosh:1");
        Affix.add("of the Magma:1");
        Affix.add("of the Volcano:1");
        Affix.add("of Ephij:1");
        Affix.add("of the Lightning:1");
        Affix.add("of the Maelstrom:1");
        Affix.add("of Haast:1");
        Affix.add("of the Ice:1");
        Affix.add("of the Polar Bear:1");
        Affix.add("of Bameth:1");
        Affix.add("of Exile:1");
        Affix.add("of Expulsion:1");
        Affix.add("of the Godslayer:1");
        Affix.add("of the Gods:1");
        Affix.add("of the Titan:1");
        Affix.add("of the Leviathan:1");
        Affix.add("of the Blur:1");
        Affix.add("of the Wind:1");
        Affix.add("of the Phantom:1");
        Affix.add("of the Jaguar:1");
        Affix.add("of the Polymath:1");
        Affix.add("of the Genius:1");
        Affix.add("of the Virtuoso:1");
        Affix.add("of the Savant:1");
        Affix.add("of Lioneye:1");
        Affix.add("of the Ranger:1");
        Affix.add("Priest's:1");
        Affix.add("Abbot's:1");
        Affix.add("Djinn's:1");
        Affix.add("Naga's:1");
        Affix.add("Necromancer's:1");
        Affix.add("Summoner's:1");
        Affix.add("Zaffre:1");
        Affix.add("Blue:1");
        
        OrCount = 4;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
       

        //GLOVES LIFE 3 MOD
        Prop = new ArrayList();
        Prop.add("ItemClass:Gloves");
        Affix = new ArrayList();
        Affix.add("of Tzteosh:1");
        Affix.add("of the Magma:1");
        Affix.add("of Ephij:1");
        Affix.add("of the Lightning:1");
        Affix.add("of Haast:1");
        Affix.add("of the Ice:1");
        Affix.add("of Bameth:1");
        Affix.add("of Exile:1");
        Affix.add("Athlete's:1");
        Affix.add("Virile:1");
        Affix.add("of the Godslayer:1");
        Affix.add("of the Gods:1");
        Affix.add("of the Titan:1");
        Affix.add("of the Leviathan:1");
        Affix.add("of the Blur:1");
        Affix.add("of the Wind:1");
        Affix.add("of the Phantom:1");
        Affix.add("of the Jaguar:1");
        Affix.add("of the Polymath:1");
        Affix.add("of the Genius:1");
        Affix.add("of the Virtuoso:1");
        Affix.add("of the Savant:1");
        
        Affix.add("of Grandmastery:1");
        Affix.add("of Mastery:1");
        Affix.add("of Lioneye:1");
        Affix.add("of the Ranger:1");
        Affix.add("Thirsty:1");
        Affix.add("Remora's:1");
        Affix.add("Prior's:1");
        Affix.add("Urchin's:1");
        Affix.add("Fawn's:1");

        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //GLOVES LIFE 4 MOD
        Prop = new ArrayList();
        Prop.add("ItemClass:Gloves");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        Affix.add("of Tzteosh:1");
        Affix.add("of the Magma:1");
        Affix.add("of the Volcano:1");
        Affix.add("of Ephij:1");
        Affix.add("of the Lightning:1");
        Affix.add("of the Maelstrom:1");
        Affix.add("of Haast:1");
        Affix.add("of the Ice:1");
        Affix.add("of the Polar Bear:1");
        Affix.add("of Bameth:1");
        Affix.add("of Exile:1");
        Affix.add("of Expulsion:1");
        Affix.add("Athlete's:1");
        Affix.add("Virile:1");
        Affix.add("Rotund:1");
        Affix.add("Robust:1");
        Affix.add("of the Godslayer:1");
        Affix.add("of the Gods:1");
        Affix.add("of the Titan:1");
        Affix.add("of the Leviathan:1");
        Affix.add("of the Blur:1");
        Affix.add("of the Wind:1");
        Affix.add("of the Phantom:1");
        Affix.add("of the Jaguar:1");
        Affix.add("of the Polymath:1");
        Affix.add("of the Genius:1");
        Affix.add("of the Virtuoso:1");
        Affix.add("of the Savant:1");
        Affix.add("of Grandmastery:1");
        Affix.add("of Mastery:1");
        Affix.add("of Lioneye:1");
        Affix.add("of the Ranger:1");
        Affix.add("Thirsty:1");
        Affix.add("Remora's:1");
        Affix.add("Honed:1");
        Affix.add("Crackling:1");
        Affix.add("Burning:1");
        Affix.add("Frigid:1");
        Affix.add("Prior's:1");
        Affix.add("Urchin's:1");
        Affix.add("Fawn's:1");
        Affix.add("Zaffre:1");
        Affix.add("Blue:1");

        OrCount = 4;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //GLOVES ES 3 MOD
        Prop = new ArrayList();
        Prop.add("ItemClass:Helmets");
        Affix = new ArrayList();
        Affix.add("Blazing:1");
        Affix.add("Seething:1");
        Affix.add("Pulsing:1");
        Affix.add("Unassailable:1");
        Affix.add("Indomitable:1");
        Affix.add("Dauntless:1");

        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //GLOVES ES 4 MOD
        Prop = new ArrayList();
        Prop.add("ItemClass:Helmets");
        Prop.add("Veiled:1:1");
        Affix = new ArrayList();
        Affix.add("Prior's:1");
        Affix.add("Deacon's:1");
        Affix.add("Seraphim's:1");
        Affix.add("Seething:1");
        Affix.add("Pulsing:1");
        Affix.add("Radiating:1");
        Affix.add("Unassailable:1");
        Affix.add("Indomitable:1");
        Affix.add("Dauntless:1");
        Affix.add("of Tzteosh:1");
        Affix.add("of the Magma:1");
        Affix.add("of the Volcano:1");
        Affix.add("of Ephij:1");
        Affix.add("of the Lightning:1");
        Affix.add("of the Maelstrom:1");
        Affix.add("of Haast:1");
        Affix.add("of the Ice:1");
        Affix.add("of the Polar Bear:1");
        Affix.add("of Bameth:1");
        Affix.add("of Exile:1");
        Affix.add("of Expulsion:1");
        Affix.add("of the Godslayer:1");
        Affix.add("of the Gods:1");
        Affix.add("of the Titan:1");
        Affix.add("of the Leviathan:1");
        Affix.add("of the Blur:1");
        Affix.add("of the Wind:1");
        Affix.add("of the Phantom:1");
        Affix.add("of the Jaguar:1");
        Affix.add("of the Polymath:1");
        Affix.add("of the Genius:1");
        Affix.add("of the Virtuoso:1");
        Affix.add("of the Savant:1");
        Affix.add("of Grandmastery:1");
        Affix.add("of Mastery:1");
        Affix.add("of Lioneye:1");
        Affix.add("of the Ranger:1");
        Affix.add("Thirsty:1");
        Affix.add("Remora's:1");
        Affix.add("Honed:1");
        Affix.add("Crackling:1");
        Affix.add("Burning:1");
        Affix.add("Frigid:1");
        Affix.add("Zaffre:1");
        Affix.add("Blue:1");

        OrCount = 4;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
//--------------------------------------------- DONE ----------------------------------------------

        //BOOTS LIFE 3 MOD MS
        Prop = new ArrayList();
        Prop.add("ItemClass:Boots");
        Affix = new ArrayList();
        Affix.add("#% increased Movement Speed:30:0");
        
        Affix.add("of Tzteosh:1");
        Affix.add("of the Magma:1");
        Affix.add("of Ephij:1");
        Affix.add("of the Lightning:1");
        Affix.add("of Haast:1");
        Affix.add("of the Ice:1");
        Affix.add("of Bameth:1");
        Affix.add("of Exile:1");
        Affix.add("Athlete's:1");
        Affix.add("Virile:1");
        Affix.add("of the Godslayer:1");
        Affix.add("of the Gods:1");
        Affix.add("of the Titan:1");
        Affix.add("of the Blur:1");
        Affix.add("of the Wind:1");
        Affix.add("of the Phantom:1");
        Affix.add("of the Polymath:1");
        Affix.add("of the Genius:1");
        Affix.add("of the Virtuoso:1");
        Affix.add("Zaffre:1");

        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //BOOTS LIFE 3 MOD Veiled
        Prop = new ArrayList();
        Prop.add("ItemClass:Boots");
        Affix = new ArrayList();
        Affix.add("Veiled:1:0");
        
        Affix.add("of Tzteosh:1");
        Affix.add("of the Magma:1");
        Affix.add("of Ephij:1");
        Affix.add("of the Lightning:1");
        Affix.add("of Haast:1");
        Affix.add("of the Ice:1");
        Affix.add("of Bameth:1");
        Affix.add("of Exile:1");
        Affix.add("Athlete's:1");
        Affix.add("Virile:1");
        Affix.add("of the Godslayer:1");
        Affix.add("of the Gods:1");
        Affix.add("of the Titan:1");
        Affix.add("of the Blur:1");
        Affix.add("of the Wind:1");
        Affix.add("of the Phantom:1");
        Affix.add("of the Polymath:1");
        Affix.add("of the Genius:1");
        Affix.add("of the Virtuoso:1");
        Affix.add("Zaffre:1");

        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //BOOTS LIFE 4 MOD
        Prop = new ArrayList();
        Prop.add("ItemClass:Boots");
        Affix = new ArrayList();
        Affix.add("#% increased Movement Speed:25:0");
        
        Affix.add("Veiled:1");
        Affix.add("of Tzteosh:1");
        Affix.add("of the Magma:1");
        Affix.add("of the Volcano:1");
        Affix.add("of Ephij:1");
        Affix.add("of the Lightning:1");
        Affix.add("of the Maelstrom:1");
        Affix.add("of Haast:1");
        Affix.add("of the Ice:1");
        Affix.add("of the Polar Bear:1");
        Affix.add("of Bameth:1");
        Affix.add("of Exile:1");
        Affix.add("of Expulsion:1");
        Affix.add("Athlete's:1");
        Affix.add("Virile:1");
        Affix.add("Rotund:1");
        Affix.add("of the Godslayer:1");
        Affix.add("of the Gods:1");
        Affix.add("of the Titan:1");
        Affix.add("of the Leviathan:1");
        Affix.add("of the Blur:1");
        Affix.add("of the Wind:1");
        Affix.add("of the Phantom:1");
        Affix.add("of the Jaguar:1");
        Affix.add("of the Polymath:1");
        Affix.add("of the Genius:1");
        Affix.add("of the Virtuoso:1");
        Affix.add("of the Savant:1");
        Affix.add("Zaffre:1");
        Affix.add("Blue:1");

        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);

        //BOOTS ES 3 MOD MS
        Prop = new ArrayList();
        Prop.add("ItemClass:Boots");
        Affix = new ArrayList();
        Affix.add("#% increased Movement Speed:25:0");
        
        Affix.add("Seething:1");
        Affix.add("Pulsing:1");
        Affix.add("Unassailable:1");
        Affix.add("Indomitable:1");
        Affix.add("Dauntless:1");
        Affix.add("of the Godslayer:1");
        Affix.add("of the Gods:1");
        Affix.add("of the Titan:1");
        Affix.add("of the Blur:1");
        Affix.add("of the Wind:1");
        Affix.add("of the Phantom:1");
        Affix.add("of the Polymath:1");
        Affix.add("of the Genius:1");
        Affix.add("of the Virtuoso:1");

        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //BOOTS ES 3 MOD Veiled
        Prop = new ArrayList();
        Prop.add("ItemClass:Boots");
        Affix = new ArrayList();
        Affix.add("Veiled:1:0");
        
        Affix.add("Seething:1");
        Affix.add("Pulsing:1");
        Affix.add("Unassailable:1");
        Affix.add("Indomitable:1");
        Affix.add("Dauntless:1");
        
        Affix.add("of the Godslayer:1");
        Affix.add("of the Gods:1");
        Affix.add("of the Titan:1");
        Affix.add("of the Blur:1");
        Affix.add("of the Wind:1");
        Affix.add("of the Phantom:1");
        Affix.add("of the Polymath:1");
        Affix.add("of the Genius:1");
        Affix.add("of the Virtuoso:1");

        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);     
        
        //BOOTS ES 4 MOD Veiled
        Prop = new ArrayList();
        Prop.add("ItemClass:Boots");
        Affix = new ArrayList();
        Affix.add("#% increased Movement Speed:25:0");
        
        Affix.add("Seething:1");
        Affix.add("Pulsing:1");
        Affix.add("Unassailable:1");
        Affix.add("Indomitable:1");
        Affix.add("Dauntless:1");
        Affix.add("of Tzteosh:1");
        Affix.add("of the Magma:1");
        Affix.add("of the Volcano:1");
        Affix.add("of Ephij:1");
        Affix.add("of the Lightning:1");
        Affix.add("of the Maelstrom:1");
        Affix.add("of Haast:1");
        Affix.add("of the Ice:1");
        Affix.add("of the Polar Bear:1");
        Affix.add("of Bameth:1");
        Affix.add("of Exile:1");
        Affix.add("of Expulsion:1");
        Affix.add("of the Godslayer:1");
        Affix.add("of the Gods:1");
        Affix.add("of the Titan:1");
        Affix.add("of the Leviathan:1");
        Affix.add("of the Blur:1");
        Affix.add("of the Wind:1");
        Affix.add("of the Phantom:1");
        Affix.add("of the Jaguar:1");
        Affix.add("of the Polymath:1");
        Affix.add("of the Genius:1");
        Affix.add("of the Virtuoso:1");
        Affix.add("of the Savant:1");

        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //BOOTS ES 4 MOD Veiled
        Prop = new ArrayList();
        Prop.add("ItemClass:Boots");
        Affix = new ArrayList();
        Affix.add("Veiled:1:0");
        
        Affix.add("Seething:1");
        Affix.add("Pulsing:1");
        Affix.add("Unassailable:1");
        Affix.add("Indomitable:1");
        Affix.add("Dauntless:1");
        Affix.add("of Tzteosh:1");
        Affix.add("of the Magma:1");
        Affix.add("of the Volcano:1");
        Affix.add("of Ephij:1");
        Affix.add("of the Lightning:1");
        Affix.add("of the Maelstrom:1");
        Affix.add("of Haast:1");
        Affix.add("of the Ice:1");
        Affix.add("of the Polar Bear:1");
        Affix.add("of Bameth:1");
        Affix.add("of Exile:1");
        Affix.add("of Expulsion:1");
        
        Affix.add("of the Godslayer:1");
        Affix.add("of the Gods:1");
        Affix.add("of the Titan:1");
        Affix.add("of the Leviathan:1");
        
        Affix.add("of the Blur:1");
        Affix.add("of the Wind:1");
        Affix.add("of the Phantom:1");
        Affix.add("of the Jaguar:1");
        
        Affix.add("of the Polymath:1");
        Affix.add("of the Genius:1");
        Affix.add("of the Virtuoso:1");
        Affix.add("of the Savant:1");

        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);   
        
        //BOOTS STATS
        Prop = new ArrayList();
        Prop.add("ItemClass:Boots");
        Affix = new ArrayList();

        Affix.add("Veiled:1");
        Affix.add("of the Veil:1");
        Affix.add("of Tzteosh:1");
        Affix.add("of the Magma:1");
        Affix.add("of Ephij:1");
        Affix.add("of the Lightning:1");
        Affix.add("of Haast:1");
        Affix.add("of the Ice:1");
        Affix.add("of Bameth:1");
        Affix.add("of Exile:1");
        Affix.add("Athlete's:1");
        
        Affix.add("of the Godslayer:1");
        Affix.add("of the Gods:1");
        Affix.add("of the Titan:1");
        
        Affix.add("of the Blur:1");
        Affix.add("of the Wind:1");
        Affix.add("of the Phantom:1");
        
        Affix.add("of the Polymath:1");
        Affix.add("of the Genius:1");
        Affix.add("of the Savant:1");

        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        

        //GENERAL RESIST GEAR
        Prop = new ArrayList();
        Prop.add("IsWeapon:0");
        Affix = new ArrayList();
        Affix.add("of Tzteosh:1");
        Affix.add("of the Magma:1");
        Affix.add("of Ephij:1");
        Affix.add("of the Lightning:1");
        Affix.add("of Haast:1");
        Affix.add("of the Ice:1");
        Affix.add("of Bameth:1");
        Affix.add("of Exile:1");
        Affix.add("of the Godslayer:1");
        Affix.add("of the Gods:1");
        Affix.add("of the Titan:1");
        Affix.add("of the Blur:1");
        Affix.add("of the Wind:1");
        Affix.add("of the Phantom:1");
        Affix.add("of the Polymath:1");
        Affix.add("of the Genius:1");
        Affix.add("of the Virtuoso:1");
        Affix.add("of the Savant:1");
        Affix.add("of the Span:1");
        Affix.add("of the Rainbow:1");

        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
 
        //MODFILTER: SPELLSLINGER WEAPONS, 3
        Prop = new ArrayList();
        Prop.add("ItemClass:Rune Daggers|Sceptres|Wands");
        Affix = new ArrayList();
        
        Affix.add("Veiled:1");
        Affix.add("Matatl's:1");
        Affix.add("Tacati:1");
        Affix.add("Topotante's:1");
        Affix.add("of the Underground:1");
        Affix.add("Subterranean:1");
        Affix.add("of Many:1");
        Affix.add("RunicxGlyphic:1");
        Affix.add("Flame Shaper's:1");
        Affix.add("Frost Singer's:1");
        Affix.add("Thunderhand's:1");
        Affix.add("Mad Lord's:1");
        Affix.add("Lithomancer's:1");
        Affix.add("Magister's:1");
        Affix.add("of Unmaking:1");
        Affix.add("of Ruinxof Destruction:1");
        Affix.add("Baleful:1");
        Affix.add("Lich's:1");
        Affix.add("Xoph's:1");
        Affix.add("Pyroclastic:1");
        Affix.add("Tul's:1");
        Affix.add("Cryomancer's:1");
        Affix.add("Esh's:1");
        Affix.add("Ionising:1");
        Affix.add("Electrocuting:1");
        Affix.add("Discharging:1");
        Affix.add("Entombing:1");
        Affix.add("Polar:1");
        Affix.add("Cremating:1");
        Affix.add("Blasting:1");
        Affix.add("Flaring:1");
        Affix.add("Carbonising:1");
        Affix.add("Crystalising:1");
        Affix.add("Vapourising:1");
        Affix.add("Merciless:1");
        Affix.add("Tyrannical:1");
        Affix.add("Cruel:1");
        Affix.add("Malicious:1");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //MODFILTER: SPELLSLINGER WEAPONS, 4
        Prop = new ArrayList();
        Prop.add("ItemClass:Rune Daggers|Sceptres|Wands");
        Affix = new ArrayList();
        
        Affix.add("Veiled:1");
        Affix.add("of the Veil:1");
        Affix.add("Matatl's:1");
        Affix.add("Tacati:1");
        Affix.add("Topotante's:1");
        Affix.add("of the Underground:1");
        Affix.add("Subterranean:1");
        Affix.add("of Many:1");
        Affix.add("Runic:1");
        Affix.add("Glyphic:1");
        Affix.add("Incanter's:1");
        Affix.add("Flame Shaper's:1");
        Affix.add("Frost Singer's:1");
        Affix.add("Thunderhand's:1");
        Affix.add("Mad Lord's:1");
        Affix.add("Lithomancer's:1");
        Affix.add("Magister's:1");
        Affix.add("of Unmaking:1");
        Affix.add("of Ruin:1");
        Affix.add("of Calamity:1");
        Affix.add("of Destruction:1");
        Affix.add("of Ferocity:1");
        Affix.add("of Fury:1");
        Affix.add("Baleful:1");
        Affix.add("Inimical:1");
        Affix.add("Lich's:1");
        Affix.add("Xoph's:1");
        Affix.add("Pyroclastic:1");
        Affix.add("Magmatic:1");
        Affix.add("Tul's:1");
        Affix.add("Cryomancer's:1");
        Affix.add("Crystalline:1");
        Affix.add("Esh's:1");
        Affix.add("Ionising:1");
        Affix.add("Smiting:1");
        Affix.add("Electrocuting:1");
        Affix.add("Discharging:1");
        Affix.add("Shocking:1");
        Affix.add("Entombing:1");
        Affix.add("Polar:1");
        Affix.add("Glaciated:1");
        Affix.add("Cremating:1");
        Affix.add("Blasting:1");
        Affix.add("Incinerating:1");
        Affix.add("Flaring:1");
        Affix.add("Tempered:1");
        Affix.add("Carbonising:1");
        Affix.add("Crystalising:1");
        Affix.add("Vapourising:1");
        Affix.add("Merciless:1");
        Affix.add("Tyrannical:1");
        Affix.add("Cruel:1");
        Affix.add("Malicious:1");
        Affix.add("of Acclaim:1");
        
        OrCount = 4;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //MODFILTER: ELE+CHAOS CASTER WEAPONS, FIRE
        Prop = new ArrayList();
        Prop.add("ItemClass:Rune Daggers|Sceptres|Wands");
        Affix = new ArrayList();
        
        Affix.add("Veiled:1");
        Affix.add("Matatl's:1");
        Affix.add("Tacati:1");
        Affix.add("Topotante's:1");
        Affix.add("of the Underground:1");
        Affix.add("Subterranean:1");
        Affix.add("of Many:1");
        Affix.add("Runic:1");
        Affix.add("Glyphic:1");
        Affix.add("Incanter's:1");
        Affix.add("Xoph's:1");
        Affix.add("Pyroclastic:1");
        Affix.add("Magmatic:1");
        Affix.add("Electrocuting:1");
        Affix.add("Discharging:1");
        Affix.add("Shocking:1");
        Affix.add("Lava Conjurer's:1");
        Affix.add("Tecton's:1");
        Affix.add("Magister's:1");
        Affix.add("of Unmaking:1");
        Affix.add("of Ruin:1");
        Affix.add("of Calamity:1");
        Affix.add("of Finesse:1");
        Affix.add("of Sortilege:1");
        Affix.add("of Destruction:1");
        Affix.add("of Ferocity:1");
        Affix.add("of Nirvana:1");
        Affix.add("Zaffre:1");
        Affix.add("Blue:1");
        Affix.add("Mazarine:1");
        Affix.add("of Liquefaction:1");
        Affix.add("of Dispersion:1");
        Affix.add("of Conflagrating:1");
        Affix.add("of Combusting:1");
        Affix.add("Carbonising:1");
        Affix.add("Cremating:1");
        Affix.add("Fanatical:1");
        Affix.add("Zealous:1");
        Affix.add("Baleful:1");
        Affix.add("Inimical:1");
        Affix.add("Corrosive:1");
        Affix.add("Dissolving:1");
        Affix.add("Ardent:1");
        Affix.add("Lich's:1");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //MODFILTER: ELE+CHAOS CASTER WEAPONS, COLD
        Prop = new ArrayList();
        Prop.add("ItemClass:Rune Daggers|Sceptres|Wands");
        Affix = new ArrayList();
        
        Affix.add("Veiled:1");
        Affix.add("Matatl's:1");
        Affix.add("Tacati:1");
        Affix.add("Topotante's:1");
        Affix.add("of the Underground:1");
        Affix.add("Subterranean:1");
        Affix.add("of Many:1");
        Affix.add("Runic:1");
        Affix.add("Glyphic:1");
        Affix.add("Incanter's:1");
        Affix.add("Tul's:1");
        Affix.add("Cryomancer's:1");
        Affix.add("Crystalline:1");
        Affix.add("Entombing:1");
        Affix.add("Polar:1");
        Affix.add("Glaciated:1");
        Affix.add("Winter Beckoner's:1");
        Affix.add("Tecton's:1");
        Affix.add("Magister's:1");
        Affix.add("of Unmaking:1");
        Affix.add("of Ruin:1");
        Affix.add("of Calamity:1");
        Affix.add("of Finesse:1");
        Affix.add("of Sortilege:1");
        Affix.add("of Destruction:1");
        Affix.add("of Ferocity:1");
        Affix.add("of Nirvana:1");
        Affix.add("Zaffre:1");
        Affix.add("Blue:1");
        Affix.add("Mazarine:1");
        Affix.add("of Liquefaction:1");
        Affix.add("of Dispersion:1");
        Affix.add("Crystalising:1");
        Affix.add("Baleful:1");
        Affix.add("Inimical:1");
        Affix.add("Mortifying:1");
        Affix.add("Festering:1");
        Affix.add("Heartstopping:1");
        Affix.add("Gelid:1");
        Affix.add("Boreal:1");
        Affix.add("Lich's:1");
        
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //MODFILTER: ELE+CHAOS CASTER WEAPONS, LIGHT
        Prop = new ArrayList();
        Prop.add("ItemClass:Rune Daggers|Sceptres|Wands");
        Affix = new ArrayList();

        Affix.add("Veiled:1");
        Affix.add("Matatl's:1");
        Affix.add("Tacati:1");
        Affix.add("Topotante's:1");
        Affix.add("of the Underground:1");
        Affix.add("Subterranean:1");
        Affix.add("of Many:1");
        Affix.add("Runic:1");
        Affix.add("Glyphic:1");
        Affix.add("Incanter's:1");
        Affix.add("Esh's:1");
        Affix.add("Ionising:1");
        Affix.add("Smiting:1");
        Affix.add("Cremating:1");
        Affix.add("Blasting:1");
        Affix.add("Incinerating:1");
        Affix.add("Tempest Master's:1");
        Affix.add("Tecton's:1");
        Affix.add("Magister's:1");
        Affix.add("of Unmaking:1");
        Affix.add("of Ruin:1");
        Affix.add("of Calamity:1");
        Affix.add("of Finesse:1");
        Affix.add("of Sortilege:1");
        Affix.add("of Destruction:1");
        Affix.add("of Ferocity:1");
        Affix.add("of Nirvana:1");
        Affix.add("Zaffre:1");
        Affix.add("Blue:1");
        Affix.add("Mazarine:1");
        Affix.add("of Liquefaction:1");
        Affix.add("of Dispersion:1");
        Affix.add("Vapourising:1");
        Affix.add("Electrocuting:1");
        Affix.add("Baleful:1");
        Affix.add("Inimical:1");
        Affix.add("Excruciating:1");
        Affix.add("Harrowing:1");
        Affix.add("Lich's :1");       
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //MODFILTER: ELE+CHAOS CASTER WEAPONS, CHAOS
        Prop = new ArrayList();
        Prop.add("ItemClass:Rune Daggers|Sceptres|Wands");
        Affix = new ArrayList();
        
        Affix.add("Veiled:1");
        Affix.add("Matatl's:1");
        Affix.add("Tacati:1");
        Affix.add("Topotante's:1");
        Affix.add("of the Underground:1");
        Affix.add("Subterranean:1");
        Affix.add("of Many:1");
        Affix.add("Runic:1");
        Affix.add("Glyphic:1");
        Affix.add("Incanter's:1");
        Affix.add("Mad Lord's:1");
        Affix.add("Tecton's:1");
        Affix.add("Magister's:1");
        Affix.add("of Unmaking:1");
        Affix.add("of Ruin:1");
        Affix.add("of Calamity:1");
        Affix.add("of Finesse:1");
        Affix.add("of Sortilege:1");
        Affix.add("of Destruction:1");
        Affix.add("of Ferocity:1");
        Affix.add("of Nirvana:1");
        Affix.add("of Euphoria:1");
        Affix.add("Zaffre:1");
        Affix.add("Blue:1");
        Affix.add("Mazarine:1");
        Affix.add("of Liquefaction:1");
        Affix.add("of Dispersion:1");
        Affix.add("Baleful:1");
        Affix.add("Inimical:1");
        Affix.add("Lich's:1");
        Affix.add("Disintegrating:1");
        Affix.add("Atrophying:1");
        Affix.add("Deteriorating:1");

        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        

        //MODFILTER: CASTER WEAPONS, 3
        Prop = new ArrayList();
        Prop.add("ItemClass:Rune Daggers|Sceptres|Wands");
        Affix = new ArrayList();
        
        Affix.add("Veiled:1");
        Affix.add("Empress's:1");
        Affix.add("Queen's:1");
        Affix.add("Martinet's:1");
        Affix.add("Matatl's:1");
        Affix.add("Tacati:1");
        Affix.add("Topotante's:1");
        Affix.add("of the Underground:1");
        Affix.add("Subterranean:1");
        Affix.add("of Many:1");
        Affix.add("Runic:1");
        Affix.add("Glyphic:1");
        Affix.add("Incanter's:1");
        Affix.add("Xoph's:1");
        Affix.add("Pyroclastic:1");
        Affix.add("Magmatic:1");
        Affix.add("Tul's:1");
        Affix.add("Cryomancer's:1");
        Affix.add("Crystalline:1");
        Affix.add("Esh's:1");
        Affix.add("Ionising:1");
        Affix.add("Smiting:1");
        Affix.add("Electrocuting:1");
        Affix.add("Discharging:1");
        Affix.add("Entombing:1");
        Affix.add("Polar:1");
        Affix.add("Cremating:1");
        Affix.add("Blasting:1");
        Affix.add("Flame Shaper's:1");
        Affix.add("Frost Singer's:1");
        Affix.add("Thunderhand's:1");
        Affix.add("Mad Lord's:1");
        Affix.add("Lithomancer's:1");
        Affix.add("Magister's:1");
        Affix.add("of Unmaking:1");
        Affix.add("of Ruin:1");
        Affix.add("of Finesse:1");
        Affix.add("of Destruction:1");
        Affix.add("Zaffre:1");
        Affix.add("Lich's:1");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //MODFILTER: CASTER WEAPONS, 4
        Prop = new ArrayList();
        Prop.add("ItemClass:Rune Daggers|Sceptres|Wands");
        Affix = new ArrayList();
        
        Affix.add("Veiled:1");
        Affix.add("of the Veil:1");
        Affix.add("Empress's:1");
        Affix.add("Queen's:1");
        Affix.add("Martinet's:1");
        Affix.add("Matatl's:1");
        Affix.add("Tacati:1");
        Affix.add("Topotante's:1");
        Affix.add("of the Underground:1");
        Affix.add("Subterranean:1");
        Affix.add("of Many:1");
        Affix.add("Runic:1");
        Affix.add("Glyphic:1");
        Affix.add("Incanter's:1");
        Affix.add("Xoph's:1");
        Affix.add("Pyroclastic:1");
        Affix.add("Magmatic:1");
        Affix.add("Tul's:1");
        Affix.add("Cryomancer's:1");
        Affix.add("Crystalline:1");
        Affix.add("Esh's:1");
        Affix.add("Ionising:1");
        Affix.add("Smiting:1");
        Affix.add("Electrocuting:1");
        Affix.add("Discharging:1");
        Affix.add("Shocking:1");
        Affix.add("Entombing:1");
        Affix.add("Polar:1");
        Affix.add("Glaciated:1");
        Affix.add("Cremating:1");
        Affix.add("Blasting:1");
        Affix.add("Incinerating:1");
        Affix.add("Flame Shaper's:1");
        Affix.add("Frost Singer's:1");
        Affix.add("Thunderhand's:1");
        Affix.add("Mad Lord's:1");
        Affix.add("Lithomancer's:1");
        Affix.add("Magister's:1");
        Affix.add("of Unmaking:1");
        Affix.add("of Ruin:1");
        Affix.add("of Calamity:1");
        Affix.add("of Finesse:1");
        Affix.add("of Sortilege:1");
        Affix.add("of Destruction:1");
        Affix.add("of Ferocity:1");
        Affix.add("of Nirvana:1");
        Affix.add("Zaffre:1");
        Affix.add("Blue:1");
        Affix.add("Mazarine:1");
        Affix.add("Lich's:1");
        Affix.add("Archmage's:1");

        OrCount = 4;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
       

        //MOD ITEMS B
        Prop = new ArrayList();
        Prop.add("ItemClass:Bows");
        Affix = new ArrayList();
        
        Affix.add("Paragon's:1");
        Affix.add("Sharpshooter's:1");

        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);

        //MOD ITEMS C
        Prop = new ArrayList();
        Prop.add("ItemClass:Rune Daggers|Sceptres|Wands");
        Affix = new ArrayList();

        Affix.add("of the Underground:1");
        Affix.add("Subterranean:1");
        Affix.add("of Many:1");
        Affix.add("Martinet's:1");
        Affix.add("Matatl's:1");
        Affix.add("Tacati:1");
        Affix.add("Topotante's:1");
        Affix.add("Magister's:1");

        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        
        //MOD ITEMS D
        Prop = new ArrayList();
        Prop.add("ItemClass:Staves");
        Affix = new ArrayList();
        
        Affix.add("of the Underground:1");
        Affix.add("Subterranean:1");
        Affix.add("of Many:1");
        Affix.add("Martinet's:1");
        Affix.add("Matatl's:1");
        Affix.add("Tacati:1");
        Affix.add("Topotante's:1");
        Affix.add("Lava Conjurer's:1");
        Affix.add("Winter Beckoner's:1");
        Affix.add("Tempest Master's:1");
        Affix.add("Splintermind's:1");
        Affix.add("Tecton's:1");

        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);

       

        //MODFILTER: ELE WEAPONS, 3
        Prop = new ArrayList();
        Prop.add("ItemBase:Ambusher|Corsair Sword|Eye Gouger|Fancy Foil|Gemini Claw|Grove Bow|Hellion's Paw|Imperial Bow|Imperial Claw|Jewelled Foil|Maraketh Bow|Spiraled Foil|Thicket Bow");
        Affix = new ArrayList();
        
        Affix.add("Veiled:1");
        Affix.add("of the Underground:1");
        Affix.add("Subterranean:1");
        Affix.add("of Many:1");
        Affix.add("Matatl's:1");
        Affix.add("Tacati:1");
        Affix.add("Topotante's:1");
        Affix.add("Carbonising:1");
        Affix.add("Cremating:1");
        Affix.add("Crystalising:1");
        Affix.add("Entombing:1");
        Affix.add("Vapourising:1");
        Affix.add("Electrocuting:1");
        Affix.add("of Celebration:1");
        Affix.add("of Incision:1");
        Affix.add("of Penetrating:1");
        Affix.add("of Destruction:1");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //MODFILTER: ELE WEAPONS, 4
        Prop = new ArrayList();
        Prop.add("ItemBase:Ambusher|Corsair Sword|Eye Gouger|Fancy Foil|Gemini Claw|Grove Bow|Hellion's Paw|Imperial Bow|Imperial Claw|Jewelled Foil|Maraketh Bow|Spiraled Foil|Thicket Bow");
        Affix = new ArrayList();
        
        Affix.add("Veiled:1");
        Affix.add("of the Underground:1");
        Affix.add("Subterranean:1");
        Affix.add("of Many:1");
        Affix.add("Matatl's:1");
        Affix.add("Tacati:1");
        Affix.add("Topotante's:1");
        Affix.add("Carbonising:1");
        Affix.add("Cremating:1");
        Affix.add("Blasting:1");
        Affix.add("Crystalising:1");
        Affix.add("Entombing:1");
        Affix.add("Polar:1");
        Affix.add("Vapourising:1");
        Affix.add("Electrocuting:1");
        Affix.add("Discharging:1");
        Affix.add("of Celebration:1");
        Affix.add("of Infamy:1");
        Affix.add("of Incision:1");
        Affix.add("of Penetrating:1");
        Affix.add("of Puncturing:1");
        Affix.add("of Destruction:1");
        Affix.add("of Ferocity:1");
        
        OrCount = 4;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        

        
        
        //MODFILTER: PHYS WEAPONS, 2
        Prop = new ArrayList();
        Prop.add("IsWeapon:1");
        Affix = new ArrayList();
        
        Affix.add("of the Underground:1");
        Affix.add("Subterranean:1");
        Affix.add("of Many:1");
        Affix.add("of Tacati:1");
        Affix.add("Tacati's:1");
        Affix.add("Flaring:1");
        Affix.add("Tempered:1");
        Affix.add("Merciless:1");
        Affix.add("Tyrannical:1");
        Affix.add("Dictator's:1");
        Affix.add("Emperor's:1");
        Affix.add("of Celebration:1");
        
        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //MODFILTER: PHYS WEAPONS, 3
        Prop = new ArrayList();
        Prop.add("IsWeapon:1");
        Affix = new ArrayList();
        
        Affix.add("Veiled:1");
        Affix.add("of the Underground:1");
        Affix.add("Subterranean:1");
        Affix.add("of Many:1");
        Affix.add("of Tacati:1");
        Affix.add("Tacati's:1");
        Affix.add("Flaring:1");
        Affix.add("Tempered:1");
        Affix.add("Razor-sharp:1");
        Affix.add("Merciless:1");
        Affix.add("Tyrannical:1");
        Affix.add("Cruel:1");
        Affix.add("Dictator's:1");
        Affix.add("Emperor's:1");
        Affix.add("Conqueror's:1");
        Affix.add("of Celebration:1");
        Affix.add("of Infamy:1");
        Affix.add("of Incision:1");
        Affix.add("of Destruction:1");
        
        OrCount = 3;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //MODFILTER: PHYS WEAPONS, 4
        Prop = new ArrayList();
        Prop.add("IsWeapon:1");
        Affix = new ArrayList();
        
        Affix.add("Veiled:1");
        Affix.add("of the Underground:1");
        Affix.add("Subterranean:1");
        Affix.add("of Many:1");
        Affix.add("of Tacati:1");
        Affix.add("Tacati's:1");
        Affix.add("Flaring:1");
        Affix.add("Tempered:1");
        Affix.add("Razor-sharp:1");
        Affix.add("Annealed:1");
        Affix.add("Merciless:1");
        Affix.add("Tyrannical:1");
        Affix.add("Cruel:1");
        Affix.add("Bloodthirsty:1");
        Affix.add("Dictator's:1");
        Affix.add("Emperor's:1");
        Affix.add("Conqueror's:1");
        Affix.add("Champion's:1");
        Affix.add("of Celebration:1");
        Affix.add("of Infamy:1");
        Affix.add("of Fame:1");
        Affix.add("of Incision:1");
        Affix.add("of Penetrating:1");
        Affix.add("of Destruction:1");
        Affix.add("of Ferocity:1");
        
        OrCount = 4;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
//--------------------------------------------- DONE ----------------------------------------------

        //MODFILTER: MAGIC - PHYSICAL WEAPONS
        Prop = new ArrayList();
        Prop.add("IsWeapon:1");
        Prop.add("RarityMagic:1");
        Affix = new ArrayList();
        
        Affix.add("Merciless:1");
        Affix.add("Tyrannical:1");
        Affix.add("of Many:1");
        
        OrCount = 1;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //MODFILTER: MAGIC - CASTER
        Prop = new ArrayList();
        Prop.add("ItemClass:Rune Daggers|Sceptres|Wands|Shields");
        Prop.add("RarityMagic:1");
        Affix = new ArrayList();
        
        Affix.add("Runic:1");
        Affix.add("Xoph's:1");
        Affix.add("Tul's:1");
        Affix.add("Esh's:1");
        Affix.add("Magister's:1");
        Affix.add("Empress's:1");
        Affix.add("Martinet's:1");

        
        OrCount = 1;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //MODFILTER: MAGIC - 35% MOVEMENT SPEED BOOTS
        Prop = new ArrayList();
        Prop.add("ItemClass:Boots");
        Prop.add("RarityMagic:1");
        Affix = new ArrayList();
        
        Affix.add("Hellion's:1");

        OrCount = 1;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //MODFILTER: MAGIC - MANA FLASKS
        Prop = new ArrayList();
        Prop.add("ItemClass:Mana Flasks");
        Prop.add("RarityMagic:1");
        Affix = new ArrayList();
        
        Affix.add("Enduring:1");
        Affix.add("of Staunching:1");
        Affix.add("of Heat:1");
        Affix.add("of Warding:1");
        Affix.add("of Reflexes:1");
        Affix.add("of Iron Skin:1");

        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //MODFILTER: MAGIC - LIFE FLASKS
        Prop = new ArrayList();
        Prop.add("ItemClass:Life Flasks");
        Prop.add("RarityMagic:1");
        Affix = new ArrayList();
        
        Affix.add("Bubbling:1");
        Affix.add("Catalysed:1");
        Affix.add("of Staunching:1");

        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //MODFILTER: MAGIC - QUICKSILVER FLASK
        Prop = new ArrayList();
        Prop.add("ItemClass:Utility Flasks");
        Prop.add("RarityMagic:1");
        Affix = new ArrayList();
        
        Affix.add("Alchemist's:1");
        Affix.add("Chemist's:1");
        Affix.add("Experimenter's:1");
        Affix.add("of Adrenaline:1");

        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
        
        //MODFILTER: MAGIC - UTILITY FLASKS
        Prop = new ArrayList();
        Prop.add("ItemClass:Utility Flasks");
        Prop.add("RarityMagic:1");
        Affix = new ArrayList();
        
        Affix.add("Alchemist's:1");
        Affix.add("Chemist's:1");
        Affix.add("Experimenter's:1");
        Affix.add("of Staunching:1");
        Affix.add("of Heat:1");
        Affix.add("of Warding:1");
        Affix.add("of Reflexes:1");
        Affix.add("of Iron Skin:1");

        OrCount = 2;
        u = new Entrada(Affix,Prop,Tab,OrCount);
        Output = teste.DoStuff(Output, u);
      
        //Copy to Clipboard
        System.out.println(Output);  
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("Output Copy to Clipboard!");  
        StringSelection stringSelection = new StringSelection(Output);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
    }
        
}
    
