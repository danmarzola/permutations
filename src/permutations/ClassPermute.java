    
package permutations;

import java.util.ArrayList;


public class ClassPermute {
    
    static class Entrada{

        public Entrada(ArrayList<String> Affix, ArrayList<String> Prop, int Tab, int OrCount) {
            this.Affix = Affix;
            this.Prop = Prop;
            this.Tab = Tab;
            this.OrCount = OrCount;
        }
        ArrayList<String> Affix;    
        ArrayList<String> Prop;
        int Tab;
        int OrCount;
    }
    
    public String DoStuff(String Json, Entrada kappa){
        String Output = new String();
        String orinput = new String();
        String eval = new String();
        int Gnumber = 1;
        String GnumberString = String.format("%03d", Gnumber);
        
        if(Json.isEmpty()){
            Gnumber = 1;
            GnumberString = String.format("%03d", Gnumber);
            Output += "{";
        }else{
            for(int b = 1; b < Json.length(); b++){
                String bstring = String.format("%03d", b);
                if(!Json.contains("Group"+bstring)){
                    Gnumber = b;
                    GnumberString = String.format("%03d", Gnumber);
                    break;
                }
            }
            Output = Json.substring(0, Json.length() - 1);
            Output += ",";
        }
        //Affix Field
        Output += "\"Group"+GnumberString+"\":{\"Affix\":[";
        for(int i = 0; i<kappa.Affix.size();i++){
            String input[] = kappa.Affix.get(i).split(":");
            if (input.length < 3){
                orinput = "1";
            }else{
                orinput = input[2];
            }
            Output += "{\"#Key\":\""+input[0]+"\",\"Eval\":\">=\",\"Min\":\""+input[1]+"\",\"OrFlag\":\""+orinput+"\"},";
        }
        if(kappa.Affix.size()>0){
            
        Output = Output.substring(0, Output.length() - 1);
        }
        
        //Data Affix
        Output += "],\"Data\":{\"OrCount\":"+kappa.OrCount+",\"StashTab\":\""+kappa.Tab+"\"},";
        //Prop Field
        Output += "\"Prop\":[";
        for(int k = 0; k<kappa.Prop.size();k++){
            String input[] = kappa.Prop.get(k).split(":");
            if (input.length < 3){
                orinput = "0";
            }else{
                orinput = input[2];
            }
            if(input[0].equalsIgnoreCase("ItemClass") || input[0].equalsIgnoreCase("ItemBase")){
                eval = "~";
            }else if(input[0].equalsIgnoreCase("PrefixCount") || input[0].equalsIgnoreCase("SuffixCount")){
                eval = "<=";
            }
            else{
                eval = "=";
            }
            
            Output += "{\"#Key\":\""+input[0]+"\",\"Eval\":\""+eval+"\",\"Min\":\""+input[1]+"\",\"OrFlag\":\""+orinput+"\"},";
        }
        Output = Output.substring(0, Output.length() - 1);
        Output += "]}}";
        Gnumber++;
        GnumberString = String.format("%03d", Gnumber);
    return Output;
    }

}

